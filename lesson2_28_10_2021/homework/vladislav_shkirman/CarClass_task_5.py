class Car:

    def car_info(self, color, model):
        print(f'This car has a {color} color and its model is {model}')

    def drive(self, kms, current_mileage, max_mileage):
        x = int(current_mileage)
        print("Result of the method with", kms, "kms")
        for i in range(1, (int(kms)+1)):
            if i + x < max_mileage:
                print("My current mileage is", i + x)
            else:
                print(f"I’m broken because of my current mileage is maximum({max_mileage})")
                break
