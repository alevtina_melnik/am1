class car:
    def __init__(self,color,model,current_mileage):
        self.color=color
        self.model=model
        self.current_mileage=int(current_mileage)
        self.max_mileage=int(current_mileage)+1000
    def info(self):
        print(f'\nColor: {self.color}\nModel: {self.model}\nCurrent mileage: '
              f'{self.current_mileage}\nMax mileage: {self.max_mileage}')
    def drive(self,mileage):
        for i in range(mileage):
            if self.current_mileage+1 > self.max_mileage:
                print('Car is DEAD')
                break
            else:
                (self.current_mileage)+=1
                print("My current mileage is:",self.current_mileage)
car1=car("Red","Porsche","190")
car1.info()
car1.drive(4)