# Class name in camel case
# file name in snake case task_5_updated.py
class Car:
    # Add max mileage
    def __init__(self, color, model, current_mileage, max_mileage=None):
        self.color = color
        self.model = model

        # remove casting to int
        self.current_mileage = current_mileage

        # add default value for mileage if any not provided
        self.max_mileage = max_mileage
        if self.max_mileage is None:
            self.max_mileage = self.current_mileage + 1000

    # Method names should contain a verb because it is an action.
    # I would name it get_info or print_info.
    # Here get/print actions that are performed in the method
    def print_info(self):
        print(f'\nColor: {self.color}\nModel: {self.model}\nCurrent mileage: '
              f'{self.current_mileage}\nMax mileage: {self.max_mileage}')

    def drive(self, mileage):
        print("Start driving")
        for i in range(mileage):
            # Compare with max mileage because we use int numbers with step=1.
            # Otherwise it better to use current_mileage >= self.max_mileage:
            if self.current_mileage == self.max_mileage:
                print('Car is DEAD')
                break
            else:
                self.current_mileage += 1
                print("My current mileage is:", self.current_mileage)


car1 = Car("Red", "Porsche", 190, 200)
car2 = Car("Black", "BMV", 500)

car1.drive(6)
car1.drive(7)
car1.print_info()

car2.drive(6)
car2.drive(7)
car2.print_info()
