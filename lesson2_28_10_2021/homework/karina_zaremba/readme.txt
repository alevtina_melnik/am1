Задание1: хорошо

Задание2: хорошо

Задание3: Хорошо

Задание4: С большего пойдет.
Есть пару моментов: Если ты введешь строку, то программа падает с ошибкой
Не обязательно проверять на >=0 после того, как ты проверила на <0.


Можно сделать так. Почитай этот код и на уроке спроси, если что-то непонятно здесь

input_value = input('Enter your number: ')

try:
    number = int(input_value)
except ValueError:
    print('Only integers are supported')
else:
    if number < 0:
        print(f'{number} is negative')
    elif number % 2 == 0:
        print(f'{number} is even')
    else:
        print(f'{number} is odd')