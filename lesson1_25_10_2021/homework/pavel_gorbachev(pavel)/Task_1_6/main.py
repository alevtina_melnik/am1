class Human:
    def walk(self, num_of_kms):
        if num_of_kms < 5:
            print('I went ' + str(num_of_kms) + ' kms')
        else:
            print('Sorry. I can’t walk so much')
